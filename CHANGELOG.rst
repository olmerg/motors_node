^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package SCOOBY_NODE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
0.1.1 (2020-09-27)
------------------
* Connect to motor through serial port
* require ros2-serial
* Contributors:Olmer,

0.1.0 (2020-09-27)
------------------
* ROS 2 Foxy Fitzroy supported
* Contributors:Olmer,


