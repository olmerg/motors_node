/*
 * O programa simula o protocolo feito para os servomotores animatics
 * ele utili
 */

#include <SparkFun_TB6612.h>
#include <string.h>
//variables rx serial
char incomingBytes[20];
size_t len=0;
//variables predefinidas en el servomotor
int m=0;
int z;
int y;
int ii=0;
int b,c;
int tmp=0;

int m2=0;
int z2;
int y2;
int ii2=0;
int b2,c2;

//port of the TB6612
#define AIN1 2
#define AIN2 4
#define PWMA 6
#define BIN1 8
#define BIN2 3
#define PWMB 7
#define STBY 9
#define DEBUG 0

// Initializing motors.  
Motor motor1 = Motor(AIN1, AIN2, PWMA, 1, STBY);
Motor motor2 = Motor(BIN1, BIN2, PWMB, 1, STBY);

void setup() {
 pinMode(12, OUTPUT);
 digitalWrite(12, LOW);
 Serial.begin(38400);
 Serial1.begin(38400);
}

void loop() {


//FIRST MOTOR
  if (Serial.available() > 10) {
    len=Serial.readBytesUntil('\n', incomingBytes, sizeof(incomingBytes) / sizeof(char) );
    //Serial.println(incomingBytes);
    //Serial.println();
    if(incomingBytes[0]=='z'){// && incomingBytes[len-1]=='1'){
      sscanf(incomingBytes,"z=%d y=%d m=%d",&z,&y,&m);
      if (DEBUG){
      Serial.print(z);
      Serial.print(' ');
      Serial.print(y);
      Serial.print(' ');
      Serial.println(m);
      }
    }
    memset( incomingBytes,0, sizeof(incomingBytes));
   }
//SECOND MOTOR
    if (Serial1.available() > 10) {
    len=Serial1.readBytesUntil('\n', incomingBytes, sizeof(incomingBytes) / sizeof(char) );
    //Serial1.println(incomingBytes);
    //Serial.println();

    if(incomingBytes[0]=='z'){// && incomingBytes[len-1]=='1'){
      
      sscanf(incomingBytes,"z=%d y=%d m=%d",&z2,&y2,&m2);
      memset( incomingBytes,0, sizeof(incomingBytes));
      if (DEBUG){
      Serial1.print(z2);
      Serial1.print(' ');
      Serial1.print(y2);
      Serial1.print(' ');
      Serial1.println(m2);
      }
    }
    memset( incomingBytes,0, sizeof(incomingBytes));
   }
//FIRST MOTOR   
  if (m==1){
   //TODO: print velocity  
   ii=0;
   c=z;
   b=y;
   m=0;
   Serial.print(b);
   Serial.print(',');
   Serial.print(millis());
   Serial.print("\n\n");
   digitalWrite(12, LOW);
    }
    else{
      if (ii>1000){//lost connection (stop)
        //brake(motor1, motor2);
        c=1;
        b=map(analogRead(A0),0,890,-10000,10000);
        if(abs(b)<800)
          c=0;
      }
        else{
          ii++;
          }
      }
      switch(c){
            case 1:
            tmp=map(b,-10000,10000,-255,255);
            motor1.drive(tmp);
            break;
            default:
            brake(motor1,motor2);
            digitalWrite(12, LOW);
            }
 
// SECOND MOTOR    
    if (m2==1){
   
   ii2=0;
   c2=z2;
   b2=y2;
   m2=0;
   Serial1.print(b2);
   Serial1.print(',');
   Serial1.print(millis());
   Serial1.print("\n\n");
    }
    else{
      if (ii2>1000){//lost connection (stop) use analog input
        c2=1;
        b2=map(analogRead(A0),0,890,-10000,10000);
        if(abs(b2)<800){ //calibrate deade zone of potentiometer :)
          c2=0;
          b2=0;
        }
        analogWrite(12,map(abs(b2),0,10000,0,255));
      }
        else{
          ii2++;
          }
      }
      switch(c2){
            case 1:
            tmp=map(b2,-10000,10000,-255,255);
            motor2.drive(tmp);
            break;
            default:
            motor2.brake();
            digitalWrite(12, LOW);
            }
  delay(1);  
 /* if (Serial.available()) {
    char a=Serial.read();
    char str[2];
    str[0]=a;
    str[1]='\0';
       Serial.print(str);
     }   */         
}
