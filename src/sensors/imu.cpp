/*******************************************************************************
* Copyright 2019 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

/* Author: Olmerg */

#include "scooby_node/sensors/imu.hpp"

using namespace lma;
using namespace scooby;

sensors::Imu::Imu(
  std::shared_ptr<rclcpp::Node> & nh,
  const std::string & imu_topic_name,
  const std::string & mag_topic_name,
  const std::string & frame_id)
: Sensors(nh, frame_id)
{
  imu_pub_ = nh->create_publisher<sensor_msgs::msg::Imu>(imu_topic_name, this->qos_);
  mag_pub_ = nh->create_publisher<sensor_msgs::msg::MagneticField>(mag_topic_name, this->qos_);

  RCLCPP_INFO(nh_->get_logger(), "Succeeded to create imu publisher");
}

void sensors::Imu::publish(
  const rclcpp::Time & now,
  std::shared_ptr<SerialWrapper> & serial_wrapper)
{
  auto imu_msg = std::make_unique<sensor_msgs::msg::Imu>();

  imu_msg->header.frame_id = this->frame_id_;
  imu_msg->header.stamp = now;

  imu_msg->orientation.w = 1.0f;

  imu_msg->orientation.x = 0.0f;

  imu_msg->orientation.y = 0.0f;

  imu_msg->orientation.z = 0.0f;

  imu_msg->angular_velocity.x = 0.0f;

  imu_msg->angular_velocity.y = 0.0f;;

  imu_msg->angular_velocity.z = 0.0f;

  imu_msg->linear_acceleration.x = 0.0f;

  imu_msg->linear_acceleration.y = 0.0f;

  imu_msg->linear_acceleration.z = 9.8f;

  auto mag_msg = std::make_unique<sensor_msgs::msg::MagneticField>();

  mag_msg->header.frame_id = this->frame_id_;
  mag_msg->header.stamp = now;

  mag_msg->magnetic_field.x = 0.0f;

  mag_msg->magnetic_field.y = 0.0f;

  mag_msg->magnetic_field.z = 0.0f;

  imu_pub_->publish(std::move(imu_msg));
  mag_pub_->publish(std::move(mag_msg));
}
