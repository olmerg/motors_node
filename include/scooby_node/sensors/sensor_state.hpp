/*******************************************************************************
* Copyright 2019 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

/* Author: Olmerg */

#ifndef SCOOBY_NODE_SENSORS_SENSOR_STATE_HPP_
#define SCOOBY_NODE_SENSORS_SENSOR_STATE_HPP_



#include "scooby_node/sensors/sensors.hpp"

namespace lma
{
namespace scooby
{
namespace sensors
{
class SensorState : public Sensors
{
 public:
  explicit SensorState(
    std::shared_ptr<rclcpp::Node> & nh,
    const std::string & topic_name = "sensor_state",
    const bool & bumper_forward = false,
    const bool & bumper_backward = false,
    const bool & cliff = false,
    const bool & sonar = false,
    const bool & illumination = false);

  void publish(
    const rclcpp::Time & now,
    std::shared_ptr<SerialWrapper> & serial_wrapper) override;

 private:
 // rclcpp::Publisher<turtlebot3_msgs::msg::SensorState>::SharedPtr pub_;

  bool bumper_forward_;
  bool bumper_backward_;
  bool cliff_;
  bool sonar_;
  bool illumination_;
};
} // sensors
} // scooby
} // lma
#endif // scooby_NODE_SENSORS_SENSOR_STATE_HPP_
